package Menus;

import Controller.ClientController;
import Controller.TalentController;
import com.company.Menu;
import com.company.Talent;

import java.util.Scanner;

public class SignupMenu extends Menu {

    private SignupMenu(){}
    private static SignupMenu Instance;
    Menu signupMenu = this;
    Scanner reader = new Scanner(System.in);
   TalentController talentRepository = new TalentController();
   ClientController clientRepository = new ClientController();
   Talent talent;

    public static SignupMenu getInstance(){
        if(Instance == null){
            Instance = new SignupMenu();
        }
        return Instance;
    }

    public void showPrompt(){
        System.out.println("Enter username: ");
        String username = reader.next();


        System.out.println("Enter email: ");
        String email = reader.next();


        System.out.println("Enter phone no: ");
        String phone = reader.next();


        talent = new Talent(1,username,email);
        talentRepository.addTalent(talent);
        talentRepository.getAllTalents();
    }



    public void start(){
        showPrompt();
        processInput();
    }

    @Override
    public void processInput(int input) {
        switch (input){
            case 1:
                TalentMenu.getInstance().start();
            case 2:
        }
    }

    public void processInput(){
        LoginMenu.getInstance().start();
    }

}
