package Menus;

import com.company.Main;
import com.company.Menu;
import com.company.MenuItem;

public class MainMenu extends Menu {

    private static MainMenu Instance;

    Menu mainMenuItem = this;

    private MainMenu() {
    }

   public static MainMenu getInstance() {
        if (Instance == null) {
            Instance = new MainMenu();
        }
        return Instance;
    }

    public void addMainMenuItem() {
        mainMenuItem = this;
        mainMenuItem.title = "       Welcome to gebeya Enterprise!";
        mainMenuItem.addMenuItem(1, "Continue as a Talent ");
        mainMenuItem.addMenuItem(2, "Continue as a Client");
        mainMenuItem.addMenuItem(0, "Exit");
    }

    public void showMainMenu() {
        mainMenuItem.showMenuItem();
    }

    public void start() {
        this.addMainMenuItem();
        this.showMainMenu();
        this.waitForInput(menuItems);

    }

    @Override
    public void processInput(int input) {
        switch (input){
            case 1:
                AuthMenu.getInstance().start();
                break;
            case 2:

                ClientMenu.getInstance().start();
                break;
            case 0:
                return;
        }
    }
}
