package Menus;

import Controller.TalentController;
import com.company.Menu;
import com.company.Talent;

import java.util.Scanner;

public class LoginMenu extends Menu {

    LoginMenu(){}
    private static LoginMenu Instance;
    Menu loginMenu = this;
    TalentController talentRepository = new TalentController();
    Talent talent;

    Scanner reader = new Scanner(System.in);

    public static LoginMenu getInstance(){
        if(Instance == null){
            Instance = new LoginMenu();
        }
        return Instance;
    }

    public void showPrompt(){
        System.out.println("  Login to the system! ");
        System.out.println("Enter username: ");
        String username = reader.next();

        System.out.println("Enter email: ");
        String email = reader.next();

        talent = new Talent(1,username,email);
        talentRepository.addTalent(talent);
        System.out.println("Sucessfull");

    }

    public void start(){
        showPrompt();
        processInput();
    }

    @Override
    public void processInput(int input) {
    }

    public void processInput(){
        TalentMenu.getInstance().start();
    }
}
