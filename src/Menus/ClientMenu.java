package Menus;

import Controller.ClientController;
import Controller.ProjectController;
import Controller.TalentController;
import com.company.Client;
import com.company.Menu;
import com.company.Talent;

import java.util.Scanner;

public class ClientMenu extends Menu {

    private ClientMenu(){}
    private static ClientMenu Instance;
    Menu clientMenu = this;
    Scanner reader = new Scanner(System.in);

    ClientController clientRepository = new ClientController();
    TalentController talentRepository = new TalentController();
    ProjectController projectRepository = ProjectController.getInstance();

    Talent talent;
    Client client;


    public static ClientMenu getInstance(){
        if(Instance == null){
            Instance = new ClientMenu();
        }
        return Instance;
    }

    public void addClientMenu(){
        clientMenu.addMenuItem(1,"Create Project");
        clientMenu.addMenuItem(2,"My Projects");
        clientMenu.addMenuItem(0,"Exit");
    }

    public void showClientMenu(){
        clientMenu.showMenuItem();
    }

    public void start(){
        addClientMenu();
        showClientMenu();
        waitForInput(menuItems);
    }

    @Override
    public void processInput(int input) {
        switch (input){
            case 1:
                CreateProjectMenu.getInstance().start();


        }
    }
}
