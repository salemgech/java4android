package Menus;

import com.company.Menu;

public class AuthMenu extends Menu {

    private AuthMenu(){}
    private static AuthMenu Instance;

  Menu authMenu = this;

    public static AuthMenu getInstance(){
        if(Instance == null){
            Instance = new AuthMenu();
        }
        return Instance;
    }

    public void addAuthMenu(){
        authMenu.addMenuItem(1,"SignUp");
        authMenu.addMenuItem(2,"Login");
        authMenu.addMenuItem(3,"Exit");
    }

    public void getAuthMenu(){
       authMenu.showMenuItem();
    }

    public void start(){
        addAuthMenu();
        getAuthMenu();
        waitForInput(menuItems);
    }


    @Override
    public void processInput(int input) {
        switch (input){
            case 1:
                SignupMenu.getInstance().start();
                break;
            case 2:
                LoginMenu.getInstance().start();

        }

    }
}
