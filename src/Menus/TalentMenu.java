package Menus;

import Controller.ClientController;
import Controller.ProjectController;
import Controller.TalentController;
import com.company.Menu;
import com.company.Project;
import com.company.Talent;

import java.util.Scanner;

public class TalentMenu extends Menu {

    private TalentMenu(){}
    private static TalentMenu Instance;
    Menu talentMenu = this;
    Scanner reader = new Scanner(System.in);
    int projectChoice;

    TalentController talentRepository = new TalentController();
    ClientController clientRepository = new ClientController();
    static ProjectController projectRepository;

    Talent talent;

    public static TalentMenu getInstance(){
        if(Instance == null){
            Instance = new TalentMenu();
        }
        return Instance;
    }

    public void addTalentMenu(){
        projectRepository = ProjectController.getInstance();
        talentMenu.addMenuItem(1,"All Projects");
        talentMenu.addMenuItem(2,"My Projects");
        talentMenu.addMenuItem(0,"Exit");
    }

    public void showProjectChoicePrompt(){

        System.out.println("Choose Project from Starting from 0 for the first Project To add to your project: ");
        projectChoice = reader.nextInt();
        System.out.println(projectChoice);

    }

    public void showTalentMenu(){
        talentMenu.showMenuItem();
    }

    public void start(){
        addTalentMenu();
        showTalentMenu();
        waitForInput(menuItems);

    }

    @Override
    public void processInput(int input) {
        switch (input){
            case 1:

                if(projectRepository.getAllProjects().size() < 1){
                    System.out.println("There is no item in the project List! ");
                    showTalentMenu();
                    waitForInput(menuItems);
                }

                showProjectChoicePrompt();
                Project choosenProject = projectRepository.getAllProjects().get(projectChoice);
                projectRepository.addToAssignedProjects(choosenProject);
                System.out.println("\nSucessfully added to your project!");
                showTalentMenu();
                waitForInput(menuItems);

            case 2:

                try{

                    projectRepository.getAllAssignedProjects();
                }
                catch (Exception e){
                    System.out.println(e.getMessage());
                }


        }
    }
}
