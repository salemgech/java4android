package Menus;

import Controller.ClientController;
import Controller.ProjectController;
import com.company.*;

import java.util.Scanner;

public class CreateProjectMenu extends Menu {

    private CreateProjectMenu(){}
    private static CreateProjectMenu Instance;
    Menu createProjectMenu = this;
    ClientController clientRepository = new ClientController();
    ProjectController projectRepository = ProjectController.getInstance();
    Talent talent;
    Client client;
    Project project;

    Scanner reader = new Scanner(System.in);

    public static CreateProjectMenu getInstance(){
        if(Instance == null){
            Instance = new CreateProjectMenu();
        }
        return Instance;
    }

    public void showPromptAndAddToProject(){
        createProjectMenu = this;
        System.out.println("    create Project!  ");
        System.out.println("Enter Project Title: ");
        String title = reader.next();

        System.out.println("Enter Project Description: ");
        String description = reader.next();

        System.out.println("Enter Language: ");
        String language = reader.next();

        System.out.println("Enter Price: ");
        int price = reader.nextInt();

        System.out.println("Enter Duration: ");
        int duration = reader.nextInt();

        System.out.println("Enter Status Flag: ");
        String flag = reader.next();

        project = new Project(title,description,language,price,duration,flag);
        projectRepository.addProject(project);


        System.out.println("Your project Sucessfully Created!\n");

        System.out.println("Press one to see all the projects!");

         createProjectMenu.addMenuItem(1,"All Project");
         createProjectMenu.addMenuItem(0,"Back");
         createProjectMenu.showMenuItem();
    }



    public void start(){
        showPromptAndAddToProject();
        waitForInput(menuItems);
        processInput();
    }

    @Override
    public void processInput(int input) {
        switch (input){
            case 1:
                projectRepository.getAllProjects();
            case 0:
                MainMenu.getInstance().start();
        }
    }

    public void processInput(){


    }
}
