package com.company;

public class Talent {

    int id;
    String name;
    String email;

    public Talent(int id, String name, String email) {
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return " Name of Talent: "+name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return " Email of Talent: " + email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return String.format("%s. %s", getName(), getEmail() );
    }
}
