package com.company;

public class Booking {

    Project project;
    Talent talent;
    Client client;

    public Booking(Project project, Talent talent, Client client) {
        this.project = null;
        this.talent = null;
        this.client = null;
    }



    public Project getProject() {
        return project;
    }

    public Talent getTalent() {
        return talent;
    }

    public Client getClient() {
        return client;
    }
}
