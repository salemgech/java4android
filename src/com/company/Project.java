package com.company;

public class Project {

    String title;
    String projectDescription;
    String language;
    int price;
    int duration;
    String statusFlag;

    public Project() {
    }

    public Project(String title, String projectDescription, String language, int price, int duration, String statusFlag) {
        this.title = title;
        this.projectDescription = projectDescription;
        this.language = language;
        this.price = price;
        this.duration = duration;
        this.statusFlag = statusFlag;
    }

    public Project(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public String getLanguage() {
        return language;
    }

    public int getPrice() {
        return price;
    }

    public int getDuration() {
        return duration;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    @Override
    public String toString() {
        return String.format("Project Title: %s \n Project Description: %s \n Language: %s \n Price: %d \n Duration: %d \n Status Flag: %s ", getTitle(),getProjectDescription(),getLanguage(),getPrice(),getDuration(),getStatusFlag());
    }
}
