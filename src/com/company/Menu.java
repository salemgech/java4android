package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class Menu {

    public String title;
    public List<MenuItem> menuItems = new ArrayList<>();
    Scanner reader = new Scanner(System.in);


    public Menu(){}

    public Menu(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addMenuItem(int choice, String description){
        MenuItem menuItem = new MenuItem(choice, description);
        menuItems.add(menuItem);
    }

    public void showMenuItem(){
        System.out.println(title);
        for (MenuItem menuItem:
            menuItems ) {
            System.out.println(menuItem);
        }
    }

    public void waitForInput(List<MenuItem> menuItems){
        System.out.println(":> ");
        int choice = reader.nextInt();

        while (choice > menuItems.size() || choice < 0){
            System.out.println("Invalid input. please try again! ");
            choice = reader.nextInt();
        }
        System.out.println( "Your input is " + choice);
        processInput(choice);

    }

    public abstract void processInput(int input);

}
