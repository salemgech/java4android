package Controller;

import com.company.Project;

import java.util.List;

public class ProjectController extends ModelController {


    Project project;
    public static ModelController<Project> projectRepository = new ModelController<>();
    public static ModelController<Project> assignedProjects = new ModelController<>();
    static ProjectController Instance;

    private ProjectController(){}

    public static ProjectController getInstance(){
        if(Instance == null){
            Instance = new ProjectController();
        }
        return Instance;
    }

    public void addProject(Project project) {

        projectRepository.addModel(project);

        Project project1 = new Project("DIARY", "Simple diary app", "English", 2000, 45, "TAKEN");
        Project project2 = new Project("MUSIC", "Simple music app", "English", 10000, 45, "NOT TAKEN");
        Project project3 = new Project("FILM", "Simple film app", "English", 70070, 45, "TAKEN");

        projectRepository.addModel(project1);
        projectRepository.addModel(project2);
        projectRepository.addModel(project3);
    }



    public void addToAssignedProjects(Project project){

        assignedProjects.addAssignedModel(project);
    }

    public List<Project> getAllAssignedProjects(){

        return assignedProjects.getAllAssignedModels();
    }


    public List<Project> getAllProjects(){

        return projectRepository.getAllModels();
    }

    public void getProject(int duration){projectRepository.getModel(duration);}


    public Project getProject(String title){
        return new Project(title);
    }


}
