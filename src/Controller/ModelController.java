package Controller;

import java.util.ArrayList;
import java.util.List;

public class ModelController<T> {

    public ModelController(){}
    private ArrayList<T> models = new ArrayList<>();

    private ArrayList<T> assignedModels = new ArrayList<>();


    public void addModel(T model){
        models.add(model);
    }

    public void addAssignedModel(T model){
        assignedModels.add(model);
    }


    public List<T> getAllModels(){
        for (T model:models
             ) {
            System.out.println(model+"\n");
        }
        return models;
    }

    public List<T> getAllAssignedModels(){
        for (T model:assignedModels
        ) {
            System.out.println(model+"\n");
        }
        return models;
    }

   public T getModel(int id){
       System.out.println("Model is " + models.get(id));
       return models.get(id);
    }





}
